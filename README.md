## NBP EXCHANGE RATE APP

### Description of the application
Application for getting currency exchange rate from NBP using Rest Api.
Application contains two modules:

- backend - this module was written in Java 11, SpringBoot Web and Security.
It is responsible for communication with *api.nbp.pl* and delivering data from it.
- frontend - this was written in Angular 12. It is responsible for presentation layer.
This layer uses angular material module.

### Running the application

The entire app can run from CLI. Modules run separately. You should use following commands to run application:

- for frontend please use `mvn spring-boot:run`

- for backend please use `ng serve --open`