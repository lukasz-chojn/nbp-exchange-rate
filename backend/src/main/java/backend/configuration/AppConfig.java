package backend.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class AppConfig {

    @Bean
    public ViewResolver viewResolver() {
        var internalResourceView = new InternalResourceViewResolver();
        internalResourceView.setCache(false);
        return internalResourceView;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
