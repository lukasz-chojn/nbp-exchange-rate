package backend.dto;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class CurrencyDto {

    @Pattern(regexp = "^[A-Z]{3}$")
    private String currencyCode;
    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}")
    private String date;
}
