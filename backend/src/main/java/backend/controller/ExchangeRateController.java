package backend.controller;

import backend.dto.CurrencyDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

@RestController
@RequestMapping("/exchange-rate")
public class ExchangeRateController {

    private RestTemplate restTemplate;
    @Value("${url}")
    private String url;
    private static final String DELIMITER = "/";
    private static final String FORMAT = "?format=json";

    @GetMapping("/{currencyCode}/{date}")
    public ResponseEntity<String> getExchangeRate(@Valid CurrencyDto currencyDto) {

        var requestedUrl = new StringBuilder();
        requestedUrl
                .append(url)
                .append(DELIMITER)
                .append(currencyDto.getCurrencyCode())
                .append(DELIMITER)
                .append(currencyDto.getDate())
                .append(FORMAT);

        return new ResponseEntity<>(restTemplate
                .getForObject(requestedUrl.toString(), String.class), HttpStatus.OK);
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
