package backend.validator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.validation.BindException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

@RestControllerAdvice
public class RestValidator {

    @ExceptionHandler(BindException.class)
    public ResponseEntity<String> constraintExceptionHandler() throws JsonProcessingException {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ObjectMapper().writeValueAsString("Error parameter date or currency code"));
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<String> dateRangeHandler() throws JsonProcessingException {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ObjectMapper().writeValueAsString("Invalid date range"));
    }
}
