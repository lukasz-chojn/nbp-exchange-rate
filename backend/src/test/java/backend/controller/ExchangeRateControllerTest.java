package backend.controller;

import backend.dto.CurrencyDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class ExchangeRateControllerTest {

    private MockMvc mockMvc;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private ExchangeRateController exchangeRateController;

    @BeforeEach
    void setUp() {
        openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(exchangeRateController).build();
    }

    @Test
    void getExchangeRate() throws Exception {
        CurrencyDto dto = currencyDto();

        MockHttpServletRequestBuilder request = get("/exchange-rate/{currencyCode}/{date}",
                dto.getCurrencyCode(), dto.getDate());

        ResultActions resultActions = mockMvc.perform(request);

        assertEquals(200, resultActions.andReturn().getResponse().getStatus());
    }

    @Test
    void getExchangeRateWithWrongData() throws Exception {
        CurrencyDto dto = currencyDtoWrongData();

        MockHttpServletRequestBuilder request = get("/exchange-rate/{currencyCode}/{date}",
                dto.getCurrencyCode(), dto.getDate());

        ResultActions resultActions = mockMvc.perform(request);

        assertEquals(404, resultActions.andReturn().getResponse().getStatus());
    }

    private CurrencyDto currencyDto() {
        CurrencyDto currencyDto = new CurrencyDto();
        currencyDto.setCurrencyCode("PLK");
        currencyDto.setDate("2021-05-02");
        return currencyDto;
    }

    private CurrencyDto currencyDtoWrongData() {
        CurrencyDto currencyDto = new CurrencyDto();
        currencyDto.setCurrencyCode(null);
        currencyDto.setDate(null);
        return currencyDto;
    }
}