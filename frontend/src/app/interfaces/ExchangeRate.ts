import {Rates} from "./Rates";

export interface ExchangeRate {
  table: string;
  currency: string;
  code: string;
  rates: Rates[];
}
