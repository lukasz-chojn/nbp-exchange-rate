export interface Rates {
  no: string;
  effectiveDate: string;
  mid: number;
}
