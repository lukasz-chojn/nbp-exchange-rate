import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material/core";
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DATE_FORMAT} from "../../util/dateFormatter/DateFormat";
import {CurrencyList} from "../../util/currency/CurrencyList";
import {DatePipe} from "@angular/common";
import * as moment from "moment";
import {ExchangeRateDateService} from "../../service/ExchangeRateDateService";
import {HttpErrorResponse} from "@angular/common/http";
import {ExchangeRate} from "../../interfaces/ExchangeRate";
import {Rates} from "../../interfaces/Rates";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../../css/bootstrap.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT},
    DatePipe
  ],
})
export class AppComponent implements OnInit {

  constructor(private datePipe: DatePipe, private service: ExchangeRateDateService) {
  }

  ngOnInit(): void {
    this.reset();
  }

  date = new FormControl(moment());
  currency = new FormControl();

  exchangeRate: ExchangeRate = new class implements ExchangeRate {
    code: string;
    currency: string;
    rates: Rates[];
    table: string;
  };
  error: string = '';
  isError: boolean = false;
  minimumDateOfExchangeRate = new Date(2002, 0, 2);
  currencyExchangeRateDateValidator = new FormControl('', Validators.required);
  currencyCodeValidator = new FormControl('', Validators.required);

  currencies = CurrencyList;

  submitForm() {
    this.service.getExchangeRate(this.currency.value, this.datePipe.transform(this.date.value, 'YYYY-MM-dd'))
      .subscribe((data: ExchangeRate) => {
          this.isError = false;
          this.exchangeRate = data;
        },
        (error: HttpErrorResponse) => {
          this.isError = true;
          this.error = error.status + ': ' + error.error;
        })
    this.reset();
  }

  reset() {
    this.date.reset();
    this.currency.reset();
  }
}
