import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateDateService {

  constructor(private http: HttpClient) {
  }

  private url = 'http://localhost:8080/exchange-rate/';

  headers = {
    'Content-Type': 'application/json',
    Accept: ['application/json; charset=UTF-8', 'application/x-www-form-urlencoded']
  };

  httpHeaders = {
    headers: new HttpHeaders(this.headers)
  };

  getExchangeRate(currencyCode: string, exchangeDate: string | null): Observable<any> {
    return this.http.get(this.url + currencyCode + '/' + exchangeDate, this.httpHeaders);
  }
}
